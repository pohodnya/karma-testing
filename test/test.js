describe('main', function () {
  describe('#endsWith()', function () {
    it('should return true when the value ends with the suffix', function () {
      expect(2-1).to.equal(1);
    });

    it('should return false when the value does not end with the suffix', function () {
      assert.equal(false, false);
    });
  });
});